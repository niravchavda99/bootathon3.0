# Assignment 2
### Aim:
Counting number of zeros behind factorial of a number
### Theory:
First we find the factorial of a number using recursive function

Then we count the number of zeros from the end.
### Procedure:
<pre>
#include&lt;stdio.h&gt;
#include&lt;conio.h&gt;

long factorial(int);
int countZeros(long);

void main()
{
    int n, zeros;
    long fact;
    
    printf("Enter a number: ");
    scanf("%d", &n);
    
    fact = factorial(n);
    
    printf("Factorial of %d is %ld\n", n, fact);
    zeros = countZeros(fact);
    
    printf("Number of zeros: %d\n", zeros);
    getch();
}
long factorial(int n)
{
    long fact = n;
    if (fact >= 1)
        return fact * factorial(n - 1);
    else
        return 1;
}
int countZeros(long num)
{
    int count = 0, mod;
    do {
        mod = num % 10;
        if (mod == 0) {
            count++;
            num /= 10;
        }
    } while (mod == 0);
    return count;
}
</pre>