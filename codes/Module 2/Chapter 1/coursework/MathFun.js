"use strict";
function sin1() {
    var t11 = document.getElementById('t11');
    var t12 = document.getElementById('t12');
    var a = Math.PI / 180 * parseFloat(t11.value);
    var b = Math.sin(a);
    t12.value = b.toString();
}
function cos1() {
    var t21 = document.getElementById('t21');
    var t22 = document.getElementById('t22');
    var a = Math.PI / 180 * parseFloat(t21.value);
    console.log(a);
    var b = Math.cos(a);
    console.log(Math.cos(a));
    t22.value = b.toString();
}
function area() {
    var a1 = document.getElementById('a1');
    var a2 = document.getElementById('a2');
    var b1 = document.getElementById('b1');
    var b2 = document.getElementById('b2');
    var c1 = document.getElementById('c1');
    var c2 = document.getElementById('c2');
    var answer = document.getElementById('answer');
    var x1 = parseFloat(a1.value);
    var y1 = parseFloat(a2.value);
    var x2 = parseFloat(b1.value);
    var y2 = parseFloat(b2.value);
    var x3 = parseFloat(c1.value);
    var y3 = parseFloat(c2.value);
    var a = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
    console.log(a);
    var b = Math.sqrt(Math.pow((x1 - x3), 2) + Math.pow((y1 - y3), 2));
    console.log(b);
    var c = Math.sqrt(Math.pow((x3 - x2), 2) + Math.pow((y3 - y2), 2));
    console.log(c);
    var s = (a + b + c) / 2;
    var area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
    answer.innerHTML = "Answer is: <br>" + area.toString();
}
function areaOfCircle() {
    var radius = document.getElementById('r');
    var answer = document.getElementById('answer');
    var r = parseFloat(radius.value);
    var a = Math.PI * Math.pow(r, 2);
    answer.innerHTML = "Area of circle with radius " + r.toString() + " is " + a.toString();
}
