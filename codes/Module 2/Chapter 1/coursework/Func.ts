function comparision() {
    let a : HTMLInputElement = <HTMLInputElement>document.getElementById('d1');
    let b : HTMLInputElement = <HTMLInputElement>document.getElementById('d2');

    let x : number = +a.value;
    let y : number = +b.value;

    if(x > y)
        alert(x.toString() + " is greater than " + y.toString());
    else if(x < y)
        alert(x.toString() + " is less than " + y.toString());
    else
        alert("Numbers are same");
}

function check() {
    let a : HTMLInputElement = <HTMLInputElement>document.getElementById('d1');
    let data : number = +a.value;
    if(isNaN(data))
        alert('Not a Number!');
    else 
        alert('Yureka!, A Number...');
}

function calculate() {
    let a : HTMLInputElement = <HTMLInputElement>document.getElementById('d1');
    let b : HTMLInputElement = <HTMLInputElement>document.getElementById('d2');
    let c : HTMLInputElement = <HTMLInputElement>document.getElementById('d3');

    let l1 : HTMLInputElement = <HTMLInputElement>document.getElementById('l1');
    let l2 : HTMLInputElement = <HTMLInputElement>document.getElementById('l2');

    let x : number = +a.value;
    let y : number = +b.value;
    let z : number = +c.value;

    l2.innerHTML = "";

    if((x == y) && (y == z)) {
        l1.innerHTML = "It is an equilateral triangle.";
    }
    else if((x == y) || (y == z) || (x == z)) {
        l1.innerHTML = "It is an isosceles triangle.";
        if((Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2)) || (Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2)) || (Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2)))
            l2.innerHTML = "It is also a right angle triangle.";
    }
    else if((x != y) && (y != z) && (x != z)) {
        l1.innerHTML = "It is an scalene triangle.";
        if((Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2)) || (Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2)) || (Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2)))
            l2.innerHTML = "It is also a right angle triangle.";
    }
}

function findRealAndImag() {
    let a : HTMLInputElement = <HTMLInputElement>document.getElementById('d1');
    let b : HTMLInputElement = <HTMLInputElement>document.getElementById('d2');
    let c : HTMLInputElement = <HTMLInputElement>document.getElementById('d3');

    let data : string = a.value;
    let real : number;
    let imag : number;

    let i : number = data.indexOf("+");

    if(i != -1) {
        real = +data.substring(0, i);
        imag = +data.substring(i + 1, data.length - 1);
        b.value = "Real Part: " + real;
        c.value = "Imag Part: " + imag;
    }
    else {
        real = +data.substring(0, data.length);
        b.value = "Real Part: " + real;
        c.value = "Imag Part: 0";
    }
}

function for_loop() {
    let count : number = 1;
    for(; count <= 100; count++) {
        console.log(count);
    }
}

function while_loop() {
    let count : number = 100;
    while(count > 0) {
        console.log(count);
        count--;
    }
}

function doWhile_loop() {
    let count : number = 100;
    do {
        console.log(count);
        count-=10;
    }while(count > 0);
}

function findFactorial() {
    let input : HTMLInputElement = <HTMLInputElement>document.getElementById('input');
    let result : HTMLInputElement = <HTMLInputElement>document.getElementById('result');
    
    let num : number = +input.value;
    var count : number = 2;
    var fact : number = 1;

    while(count <= num) {
        fact *= count;
        count++;
    }
    result.value = "Factorial of " + num.toString() + " is " + fact.toString();
}

function dynamic() {
    
    let input : HTMLInputElement = <HTMLInputElement>document.getElementById('input');
    let table : HTMLTableElement = <HTMLTableElement>document.getElementById('table_1');

    let count : number = 1;
    let num : number = +input.value;

    while(table.rows.length > 1) {
        table.deleteRow(1);
    }

    for(count = 1; count <= num; count++) {
        let row : HTMLTableRowElement = table.insertRow();
        var cell : HTMLTableDataCellElement = row.insertCell();
        var text : HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = count.toString();
        cell.appendChild(text);

        var cell : HTMLTableDataCellElement = row.insertCell();
        var text : HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = (count * count).toString();
        cell.appendChild(text);
    }
}

function dynamic2() {
    
    let input : HTMLInputElement = <HTMLInputElement>document.getElementById('input');
    let table : HTMLTableElement = <HTMLTableElement>document.getElementById('table_1');

    let count : number = 1;
    let num : number = +input.value;

    while(table.rows.length > 1) {
        table.deleteRow(1);
    }

    for(count = 1; count <= num; count++) {
        var row : HTMLTableRowElement = table.insertRow();
        var cell : HTMLTableDataCellElement = row.insertCell();
        
        var text : HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.id = "t"+count;
        text.style.textAlign = "center";
        text.style.background = "yellow";

        text.value = count.toString();
        cell.appendChild(text);

        var cell : HTMLTableDataCellElement = row.insertCell();
        
        var text : HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.id = "tt"+count;
        text.style.textAlign = "center";
        text.style.background = "yellow";

        cell.appendChild(text);
    }
}

function squares() {
    let table : HTMLTableElement = <HTMLTableElement>document.getElementById('table_1');
    var count : number = 1;
    while(count < table.rows.length) {
        var row_num : HTMLInputElement = <HTMLInputElement>document.getElementById("t"+count);
        var num : number = +row_num.value;
        var square_num : HTMLInputElement = <HTMLInputElement>document.getElementById("tt"+count);
        square_num.value = (num * num).toString();
        count++;
    }
}